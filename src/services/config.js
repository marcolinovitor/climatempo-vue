import axios from 'axios';
import { enviroments } from '../environments/environments';

export const http = axios.create({
    baseURL: enviroments.climatempo.api
})