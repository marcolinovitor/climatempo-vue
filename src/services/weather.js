import { http } from './config';
import { enviroments } from '../environments/environments';

export default {
    
    getCidades: (estado) => {
        const urlPath = `locale/city?state=${estado}&token=${enviroments.climatempo.token}`;
        return http.get(urlPath);
    },

    getClima: (idCidade) => {
        const urlPath = `weather/locale/${idCidade}/current?token=${enviroments.climatempo.token}`;
        return http.get(urlPath);
    }

}